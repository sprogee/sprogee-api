<?php

$api = app(Dingo\Api\Routing\Router::class);

$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function($api) {
    $api->post('token', 'TokenController@store');
    $api->post('users', 'UserController@store');

    $api->group(['middleware' => 'auth:api'], function($api) {
    	$api->get('users/me', 'UserController@me');
        $api->resource('users', 'UserController', ['only' => ['index', 'show']]);
        $api->resource('posts', 'PostController', ['only' => ['index', 'store', 'show']]);
        $api->resource('tags', 'TagController', ['only' => ['index']]);
        $api->resource('proposals', 'ProposalController', ['only' => ['index', 'store', 'show']]);
    });
});

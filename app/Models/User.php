<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use Notifiable;

    protected $dates = ['birth_date'];

    protected $fillable = [
        'category',
        'email',
        'first_name',
        'last_name',
        'mobile',
        'password',
        'birth_date',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function proposals()
    {
        return $this->hasMany(Proposal::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asset extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'filename',
        'name',
        'public_url',
        'size',
        'type',
    ];

    public function proposals()
    {
        return $this->belongsToMany(Proposal::class);
    }
}

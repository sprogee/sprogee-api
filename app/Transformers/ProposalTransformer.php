<?php

namespace App\Transformers;

use App\Models\Proposal;
use App\Models\User;
use League\Fractal\TransformerAbstract;

class ProposalTransformer extends TransformerAbstract
{
    public function transform(Proposal $proposal)
    {
        return [
            'id' => $proposal->id,
            'user' => $this->getUser($proposal->user),
            'amount' => $proposal->amount,
            'coverLetter' => $proposal->cover_letter,
            'createdAt' => $proposal->created_at->toRfc3339String(),
            'updatedAt' => $proposal->updated_at->toRfc3339String(),
        ];
    }

    private function getUser(User $user)
    {
        return [
            'id' =>  $user->id,
            'firstName' => $user->first_name,
            'lastName' => $user->last_name,
        ];
    }
}

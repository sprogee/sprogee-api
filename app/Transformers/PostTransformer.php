<?php

namespace App\Transformers;

use App\Models\Post;
use App\Models\User;
use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'proposals',
        'tags'
    ];

    public function transform(Post $post)
    {
        return [
            'id' => $post->id,
            'user' => $this->getUser($post->user),
            'title' => $post->title,
            'description' => $post->description,
            'tags' => $post->tags->pluck('name'),
            'createdAt' => $post->created_at->toRfc3339String(),
            'updatedAt' => $post->updated_at->toRfc3339String(),
        ];
    }

    public function includeProposals(Post $post)
    {
        return $this->collection($post->proposals, new ProposalTransformer, 'proposals');
    }

    public function includeTags(Post $post)
    {
        return $this->collection($post->tags, new TagTransformer, 'tags');
    }

    private function getUser(User $user)
    {
        return [
            'id' =>  $user->id,
            'category' => $user->category,
            'firstName' => $user->first_name,
            'lastName' => $user->last_name,
            'mobile' => $user->mobile,
            'birthDate' => $user->birth_date->toDateString(),
            'createdAt' => $user->created_at->toRfc3339String(),
            'updatedAt' => $user->updated_at->toRfc3339String(),
        ];
    }
}

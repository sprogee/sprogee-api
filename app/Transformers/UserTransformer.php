<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'category' => $user->category,
            'firstName' => $user->first_name,
            'lastName' => $user->last_name,
            'email' => $user->email,
            'mobile' => $user->mobile,
            'birthDate' => $user->birth_date->toDateString(),
            'createdAt' => $user->created_at->toRfc3339String(),
            'updatedAt' => $user->updated_at->toRfc3339String()
        ];
    }
}

<?php

namespace App\Http\Requests\User;

use Dingo\Api\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'category' => 'required|in:sponsor,sponsoree',
            'email' => 'required|email|unique:users,email',
            'firstName' => 'required|max:100',
            'lastName' => 'required|max:100',
            'mobile' => 'required',
            'password' => 'required',
            'birthDate' => 'required|date',
        ];
    }
}

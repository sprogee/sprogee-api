<?php

namespace App\Http\Requests\Post;

use Dingo\Api\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required|max:255',
            'tags' => 'required|array',
            'tags.*' => 'distinct',
        ];
    }
}

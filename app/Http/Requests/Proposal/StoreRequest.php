<?php

namespace App\Http\Requests\Proposal;

use Dingo\Api\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'post' => 'required|exists:posts,id',
            'amount' => 'required|numeric',
            'coverLetter' => 'required|min:6|max:1000',
        ];
    }
}

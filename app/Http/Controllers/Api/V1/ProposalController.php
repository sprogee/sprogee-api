<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Proposal\StoreRequest;
use App\Models\Proposal;

class ProposalController extends Controller
{
	public function __construct()
	{
		$this->model = Proposal::class;
	}

	public function store()
	{
		$request = app(StoreRequest::class);

		$proposal = $request->user()
			->proposals()
			->create([
				'post_id' => $request->post,
				'amount' => $request->amount,
				'cover_letter' => $request->coverLetter,
			]);

		return $this->item($proposal)->addMeta('message', 'Proposal has been created.');
	}
}

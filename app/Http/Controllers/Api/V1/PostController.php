<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Post\StoreRequest;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public function __construct()
    {
        $this->model = Post::class;
    }

    public function index()
    {
        $posts = $this->getModel()->query()
            ->when($title = request()->title, function ($query) use ($title) {
                return $query->where('title', 'like', "%{$title}%" );
            })
            ->get();

        return $this->collection($posts);
    }

    public function store()
    {
        $request = app(StoreRequest::class);

        return DB::transaction(function() use ($request) {
            $post = $this->getModel()->create([
                'user_id' => $request->user()->id,
                'title' => $request->title,
                'description' => $request->description
            ]);

            collect($request->tags)->each(function($tag) use ($post) {
                $capitalizedTag = strtoupper($tag);
                $findTag = Tag::updateOrCreate(['name' => $capitalizedTag], [
                    'name' => $capitalizedTag
                ]);
                $post->tags()->attach($findTag->id);
            });

            return $this->item($post)->addMeta('message', 'Post has been created.');
        });
    }
}

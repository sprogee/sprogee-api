<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\User\StoreRequest;
use App\Models\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->model = User::class;
    }

    public function index()
    {
        $query = $this->getModel()->query()
            ->when(request()->has('excludeMe'), function($query) {
                return $query->whereNotIn('id', [request()->user()->id]);
            });

        return $this->collection($query->get());
    }

    public function me()
    {
        return $this->item(request()->user());
    }

    public function store()
    {
        $request = app(StoreRequest::class);

        $user = $this->getModel()->create([
            'category' => $request->category,
            'email' => $request->email,
            'first_name' => $request->firstName,
            'last_name' => $request->lastName,
            'mobile' => $request->mobile,
            'birth_date' => $request->birthDate,
            'password' => bcrypt($request->password)
        ]);

        return $this->item($user);
    }
}

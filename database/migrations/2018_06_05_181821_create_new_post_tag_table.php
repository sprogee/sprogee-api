<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewPostTagTable extends Migration
{
    const TABLE = 'post_tag';

    public function up()
    {
        Schema::dropIfExists('post_tags');
        Schema::create(self::TABLE, function (Blueprint $table) {
            $table->integer('post_id')->nullable()->unsigned();
            $table->integer('tag_id')->nullable()->unsigned();

            $table->foreign('post_id')
                  ->references('id')->on('posts')
                  ->onDelete('cascade');

            $table->foreign('tag_id')
                  ->references('id')->on('tags')
                  ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists(self::TABLE);
    }
}

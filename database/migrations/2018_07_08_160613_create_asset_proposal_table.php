<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetProposalTable extends Migration
{
    const TABLE = 'asset_proposal';

    public function up()
    {
        Schema::create(self::TABLE, function(Blueprint $table) {
            $table->integer('asset_id')->unsigned();
            $table->integer('proposal_id')->unsigned();

            $table->foreign('asset_id')
                ->references('id')
                ->on('assets')
                ->onDelete('cascade');

            $table->foreign('proposal_id')
                ->references('id')
                ->on('proposals')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop(self::TABLE);
    }

}

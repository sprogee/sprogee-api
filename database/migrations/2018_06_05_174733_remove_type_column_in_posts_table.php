<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTypeColumnInPostsTable extends Migration
{
    const TABLE = 'posts';

    public function up()
    {
        Schema::table(self::TABLE, function (Blueprint $table) {
            $table->dropColumn(['type']);
        });
    }

    public function down()
    {
        Schema::table(self::TABLE, function (Blueprint $table) {
            $table->enum('type', ['sponsor', 'sponsoree'])->nullable()->after('user_id');
        });
    }
}
